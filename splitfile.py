fileToSplitName = "indoor_EMOT_2016-07-28-2016-09-14"
fileExtension = ".xml"
numberOfLinesInSplitFile = 1000000;
fileToSplit = open(fileToSplitName + fileExtension, "r")

print "Splitting started, please wait..."
outputFile = open(fileToSplitName + "_" + str(0) + fileExtension, "a")

for i,l in enumerate(fileToSplit):
    outputFile.write(l)

    if (i % numberOfLinesInSplitFile) == 0:
        print str(i) + " lines has been processed."
        outputFile.close()
        fCounter = i // numberOfLinesInSplitFile
        outputFile = open(fileToSplitName + "_" + str(fCounter) + fileExtension, "a")

if not outputFile.closed:
    outputFile.close