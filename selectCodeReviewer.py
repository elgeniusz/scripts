import random

developers = ["Tobi", "Marcos", "Fahmi", "Edvinas", "Armin", "Slawomir"]
random.seed()
index = random.randrange(0, len(developers) , 1)

print("The following developer has been selected to do a code review of your work: ")
print("")
print(developers[index])
print("")
input("Press any key to exit...")
