def hamming_distance(x, y):
    count, z = 0, x ^ y
    while z:
        count += 1
        z &= z-1  # magic!

    return count